package ru.chelnokov.findIdentifiers;

import java.io.*;
import java.util.regex.*;

/**
 * Класс для поиска идентификаторов в текстовом файле/коде
 * и записи их в другом текстовом файле
 *
 * Идентификатор - это имя объекта в программе, которое может
 * состоять из латинских букв (строчных или заглавных), цифр от 0 до 9 и
 * знака подчеркивания. Идентификатор не должен начинаться с цифры.
 *
 * @author Chelnokov E.I./ 16IT18K
 */

public class IdentifiersSearch {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\chelnokov\\demoCode\\LuckyTicket.java"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\identifiers.txt"))) {
            String code;
            while ((code = bufferedReader.readLine()) != null) {
                String[] dataArray = code.split(" ");
                for (String aDataArray : dataArray) {
                    Pattern pattern = Pattern.compile("\\b[A-Za-z_]\\w*\\b");
                    if(isIdentifier(aDataArray, pattern)){
                        System.out.println(aDataArray);
                        bufferedWriter.write(aDataArray +" \n");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * метод для проверки слова на признаки идентификатора
     * @param aDataArray слово из прочитанной строки
     * @param pattern переменная с регулярным выражением
     * @return найденные идентификаторы
     */
    private static boolean isIdentifier(String aDataArray, Pattern pattern) {
        Matcher matcher = pattern.matcher(aDataArray);
        return matcher.matches();
    }
}
