package ru.chelnokov.demoCode;

import java.io.*;

/**
 * Класс, который ищет те самые "Счастливые билеты"
 * @author Chelnokov E.I., 16IT18K
 */

public class LuckyTicket {
    public static void main(String[] args) {
        try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream("src\\int6data.dat"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\lucky.txt"))) {
            String data;
            while (dataInputStream.available() != 0) {
                data = dataInputStream.readUTF();
                String[] stillStringData = data.split("");
                int[] intData = new int[6];
                for (int i = 0; i < intData.length; i++) {
                    intData[i] = Integer.valueOf(stillStringData[i]);
                }
                if (isLucky(intData)) {
                    bufferedWriter.write(data + "\n");
                }
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * проверяет "счастливость билета"
     * @param intData - целочисленный массив из номера билета
     * @return true, если билет счастливый, иначе false
     */
    private static boolean isLucky(int[] intData) {
        return (intData[0] + intData[1] + intData[2]) == (intData[3] + intData[4] + intData[5]);
    }
}
